$(function () {
  var availableTags = [
    "Men Shoes",
    "Kids Shoes",
    "Women Shoes",
    "Casual Shoes For Men"
  ];

  var addedEmails = [];

  $("#search_tags").autocomplete({
    source: availableTags,
    minLength: 0,
    classes: {
      "ui-autocomplete": "highlight"
    }
  })
    .focus(function () {
      $(this).autocomplete('search', $(this).val());
    });

  $('#ei-slider').eislideshow({
    easing: 'easeOutExpo',
    titleeasing: 'easeOutExpo',
    titlespeed: 1200
  });

  // Submit button
  $('#emailSubmitBtn').click(function () {
    var emailAddress = $('#emailInput').val();

    var emailObj = { emailAddress: emailAddress };

    //Add in list 
    addedEmails.push(emailObj);

    //Clear email input
    $('#emailInput').val('');

    alert('Email saved. Check your console log')
    //Log in console email added list
    addedEmails.forEach(function (item) {
      console.log(item);
    });
  });

  //Hanburger nav  
  $('#hamb-nav').click(function () {
    $('#hamb-menu').slideToggle();
  });

  // Modal login&reg window
  $("#trigger_id").leanModal({
    top: 200,
    overlay: 0.7,
    closeButton: ".modal_close"

  });

  $(function () {
    // Modal register button
    $("#register_form").click(function () {
      $(".user_login").hide();
      $(".user_register").show();
      $(".popup_title").text('Register');
      return false;
    });

    // Modal back button
    $("#back_btn").click(function () {
      $(".user_register").hide();
      $(".user_login").show();
      $(".popup_title").text('Login');
      return false;
    });
  });

  $("#modal").draggable();

  // Blog fade
  (function () {

    var fade = $(".fade");
    var fadeIndex = -1;

    function showNextFade() {
      ++fadeIndex;
      fade.eq(fadeIndex % fade.length)
        .fadeIn(1600)
        .delay(5000)
        .fadeOut(1600, showNextFade);
    }

    showNextFade();

  })();

  // Twitter widget
  window.twttr = (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function (f) {
      t._e.push(f);
    };

    return t;
  }(document, "script", "twitter-wjs"));

  // Shop by buttons
  $(".salutation").selectmenu();

  $(".salutation").each(function (index, element) {
    $(this).find("option").eq(0).remove();
  });

  // Grid-list view
  $('a.hide').on('click', function (e) {
    if ($(this).hasClass('grid')) {
      $('#main-products').removeClass('m3grids-list').addClass('m3grids-grid');
    }
    else if ($(this).hasClass('list')) {
      $('#main-products').removeClass('m3grids-grid').addClass('m3grids-list');
    }
  });

  // Prevent default action
  $('a.hide').click(function (event) {
    event.preventDefault();

  });

  // ScrollUp button
  $(document).ready(function () {

    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
      } else {
        $('.scrollup').fadeOut();
      }
    });

    $('.scrollup').click(function () {
      $("html, body").animate({ scrollTop: 0 }, 600);
      return false;
    });

  });

  // Google map
  $(document).ready(function () {
    var map = new GMaps({
      el: '#map',
      lat: 51.5073346,
      lng: -0.1276831,
    });
  });


  // Parallax background
  var $el = $('.parallax-background');
  $(window).on('scroll', function () {
    var scroll = $(document).scrollTop();
    $el.css({
      'background-position': '50% ' + (-.3 * scroll) + 'px'
    });
  });

  // List gird active buttons
  $(".hide").click(function () {
    $(".hide").removeClass("active");
    $(this).addClass("active");
  });

});




